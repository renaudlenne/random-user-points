FROM elixir:1.13

RUN apt-get update && \
    apt-get install -y postgresql-client && \
    apt-get install -y inotify-tools

ENV UNAME=phoenix
ENV UID=1000
ENV GID=1000
ENV APP_HOME /app

RUN groupadd -g $GID -o $UNAME && \
    useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME

RUN mkdir $APP_HOME && \
    chown $UID:$GID $APP_HOME

USER $UNAME

RUN mix local.hex --force && \
    mix local.rebar --force

WORKDIR $APP_HOME

CMD ["/app/entrypoint.sh"]
