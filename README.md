# Random User Points
A phoenix api app, with a single endpoint.
This return, at max 2 (it can return less), users with more than a random number of points.

## Objective
* This application
  - seeds 1,000,000 users in a database
  - keeps a state of `{max_number, timestamp}` keeping the last time the API was queries and
  the points a user should beat to be returned by the API
* Each minute
  - every user have its `points` field updated to a random number between [0-100]
  - a new random `max_number` (between [0-100]) is generated
* There is only one API endpoint `/` which
  - returns a max of 2 users whose `points` is above the `max_number` in the application state
  - returns the timestamp of the previous call to this method
  - updates the state `timestamp` with the current timestamp

## How to run the application?

### With `docker`
#### Requirements
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://github.com/docker/compose#where-to-get-docker-compose)

#### Run
Simply execute `docker-compose up` and it will fetch the images, build the container,
run them, create the database and launch the application.


### Without `docker`
#### Prerequisite
* A [PostgreSQL](https://www.postgresql.org/) (only tested with v14, but should work with most version) database running locally with a `postgres:postgres`
user with sufficient grants to create a database
* [Elixir](https://elixir-lang.org/install.html) v1.12 or above
* (on Linux) [inotify-tools](https://github.com/inotify-tools/inotify-tools/wiki), to have Live Reloading working

#### Installation
1. Install dependencies with `mix deps.get`
2. Create, migrate and seed the database with `mix ecto.setup`

#### Run
Finally, start the application with `mix phx.server`.

## Usage
No matter the way the application was launched, it should now be available on [`localhost:4000`](http://localhost:4000).

```shell
% curl -s http://localhost:4000/  | jq                                                                                                                           ✗ main:ec6145f - random-user-points 
{
  "timestamp": null,
  "users": [
    { "id": 880070, "points": 95 },
    { "id": 880080, "points": 98 }
  ]
}
```

## Tests
To run tests, simply start `mix test`
```
% mix test                                                                                                                              ✗ main:ec6145f - random-user-points 
Compiling 21 files (.ex)
Generated random_user_points app
.............

Finished in 0.1 seconds (0.07s async, 0.07s sync)
13 tests, 0 failures

Randomized with seed 249108
```

## Documentation
Documentation can be generated using `mix docs`.

It can then be found in `html` and `epub` format in the `doc` directory.