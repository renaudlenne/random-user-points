#!/bin/bash
# Docker entrypoint script.

# Update dependencies
mix deps.get

# Wait until Postgres is ready.
while ! pg_isready -q -h $PGHOST -p $PGPORT -U $PGUSER
do
  echo "$(date) - waiting for database to start"
  sleep 2
done

# Create, migrate, and seed database if it doesn't exist.
if [[ -z `psql -Atqc "\\list $PGDATABASE"` ]]; then
  echo "Database $PGDATABASE does not exist. Initializing..."
  mix ecto.setup
  echo "Database $PGDATABASE initialized."
fi

exec mix phx.server
