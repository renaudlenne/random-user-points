defmodule RandomUserPoints.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      RandomUserPoints.Repo,
      # Start the Telemetry supervisor
      RandomUserPointsWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: RandomUserPoints.PubSub},
      # Start the Endpoint (http/https)
      RandomUserPointsWeb.Endpoint,
      # Start a worker by calling: RandomUserPoints.Worker.start_link(arg)
      # {RandomUserPoints.Worker, arg}
      RandomUserPoints.Ticker
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RandomUserPoints.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    RandomUserPointsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
