defmodule RandomUserPoints.Repo do
  use Ecto.Repo,
    otp_app: :random_user_points,
    adapter: Ecto.Adapters.Postgres
end
