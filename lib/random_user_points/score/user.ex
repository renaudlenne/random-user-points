defmodule RandomUserPoints.Score.User do
  @moduledoc """
  User model definition
  """
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
               points: integer(),
               id: integer(),
               inserted_at: NaiveDateTime.t(),
               updated_at: NaiveDateTime.t()
             }

  schema "users" do
    field :points, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_required([:points])
  end
end
