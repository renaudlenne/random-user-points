defmodule RandomUserPoints.Ticker do
  @moduledoc """
  Periodically updates user points and holds state for the score to beat and last query time
  """
  use GenServer
  require Logger

  import Ecto.Query, warn: false
  alias RandomUserPoints.Repo
  alias RandomUserPoints.Score
  alias RandomUserPoints.Score.User

  @typep state :: %{max_number: integer, timestamp: DateTime.t()}
  @typep noreply_response :: {:noreply, state}
  @typep reply_response :: {:reply, reply :: term, state} | noreply_response

  def start_link(_arg, opts \\ []) do
    name = Keyword.get(opts, :name, Ticker)
    max_number = :rand.uniform(100) - 1
    Logger.debug("Ticker starting, initial max_number: #{max_number}")
    GenServer.start_link(__MODULE__, %{max_number: max_number, timestamp: nil}, name: name)
  end

  @doc """
  Generate new points for all users and update them in the database, then schedule the next tick.

  This is intended to be run asynchronously in the background
  """
  def update_all_points(caller) do
    Logger.debug("Updating points...")

    User
    |> update(
      set: [
        points: fragment("floor(random() * 100)::int"),
        updated_at: fragment("now()")
      ]
    )
    |> Repo.update_all([], timeout: 120_000) # we know this will be a long-running query

    GenServer.cast(caller, :schedule_next_tick)
  end

  @impl GenServer
  @doc false
  def init(state) do
    GenServer.cast(self(), :schedule_next_tick)
    {:ok, state}
  end

  @impl GenServer
  @spec handle_call(call :: term, GenServer.from(), state) :: reply_response
  def handle_call(
        :fetch_selected_users,
        _from,
        %{max_number: max_number, timestamp: timestamp} = state
      ) do
    users = Score.list_n_users_above_score(max_number)
    {:reply, %{users: users, timestamp: timestamp}, %{state | timestamp: DateTime.utc_now()}}
  end

  @impl GenServer
  @spec handle_info(info :: term, state) :: noreply_response
  def handle_info(:update_scores, state) do
    spawn(RandomUserPoints.Ticker, :update_all_points, [self()])
    max_number = :rand.uniform(100) - 1
    Logger.debug("New max_number: #{max_number}")
    {:noreply, %{state | max_number: max_number}}
  end

  @impl GenServer
  @spec handle_cast(cast :: term, state) :: noreply_response
  def handle_cast(:schedule_next_tick, state) do
    Process.send_after(self(), :update_scores, 60_000)
    {:noreply, state}
  end
end
