defmodule RandomUserPointsWeb.HighScoreController do
  use RandomUserPointsWeb, :controller

  action_fallback RandomUserPointsWeb.FallbackController

  def index(conn, _params) do
    reply = GenServer.call(Ticker, :fetch_selected_users)
    render(conn, "index.json", reply)
  end
end
