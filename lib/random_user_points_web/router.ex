defmodule RandomUserPointsWeb.Router do
  use RandomUserPointsWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RandomUserPointsWeb do
    pipe_through :api
    get "/", HighScoreController, :index
  end
end
