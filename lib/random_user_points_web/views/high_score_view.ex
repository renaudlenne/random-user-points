defmodule RandomUserPointsWeb.HighScoreView do
  use RandomUserPointsWeb, :view
  alias RandomUserPointsWeb.UserView

  def render("index.json", %{timestamp: timestamp, users: users}) do
    %{users: render_many(users, UserView, "user.json"), timestamp: timestamp}
  end
end
