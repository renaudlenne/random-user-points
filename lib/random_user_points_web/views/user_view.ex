defmodule RandomUserPointsWeb.UserView do
  use RandomUserPointsWeb, :view

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      points: user.points
    }
  end
end
