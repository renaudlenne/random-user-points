defmodule RandomUserPoints.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :points, :integer

      timestamps [
        default: fragment("now()") # because we want direct insert through SQL to fill timestamps columns
      ]
    end
  end
end
