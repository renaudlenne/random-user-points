# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RandomUserPoints.Repo.insert!(%RandomUserPoints.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Ecto.Multi
alias RandomUserPoints.Repo
alias RandomUserPoints.Score.User

1..1000000
|> Enum.map(fn _ -> %{points: 0} end)
|> Enum.chunk_every(20_000) # because PostgreSQL is limited to 65536 arguments
|> Enum.with_index() # to be able to identify each chunk in the Multi
|> Enum.reduce(
     Multi.new() |> Multi.delete_all(:delete_all, User), # first delete every users
     fn {chunk, idx}, multi ->
       Multi.insert_all(multi, {:insert_chunk, idx}, User, chunk) # then insert each chunk
     end
   )
|> Repo.transaction(timeout: 120_000) # run all database operations in the same transaction