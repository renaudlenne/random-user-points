defmodule RandomUserPoints.TickerTest do
  use RandomUserPoints.DataCase

  import RandomUserPoints.ScoreFixtures

  alias RandomUserPoints.Ticker
  alias RandomUserPoints.Score.User

  setup do
    child_spec = %{
      id: TestTicker,
      start: {Ticker, :start_link, [[], [name: TestTicker]]}
    }

    %{pid: start_supervised!(child_spec)}
  end


  describe "Ticker" do
    test "fetch_selected_users should return users with higher points and update state's timestamp", %{pid: pid} do
      # Initial `max_number` should be in the [0-100] range
      %{max_number: initial_max_number} = :sys.get_state(pid)
      assert initial_max_number < 100
      assert initial_max_number >= 0

      # One user above `max_number`, one below
      high_points_user = user_fixture(points: initial_max_number + 1)
      user_fixture(points: initial_max_number - 1)

      # Only the one above should be returned and the timestamp should be nil because it's the first call
      assert GenServer.call(TestTicker, :fetch_selected_users) == %{timestamp: nil, users: [high_points_user]}

      # Timestamp should have been updated in the state
      %{timestamp: updated_timestamp} = :sys.get_state(pid)
      assert updated_timestamp != nil

      # A second call should return the newly updated timestamp
      assert GenServer.call(TestTicker, :fetch_selected_users) == %{timestamp: updated_timestamp, users: [high_points_user]}
    end

    test "update_all_points should update the points of every users", %{pid: pid} do
      %User{points: initial_points, id: user_id} = user_fixture()
      Ticker.update_all_points(pid)
      %User{points: updated_points} = RandomUserPoints.Repo.get!(User, user_id)
      assert initial_points != updated_points # non-optimal, this could fail if the random number is the same as it was before
    end
  end
end