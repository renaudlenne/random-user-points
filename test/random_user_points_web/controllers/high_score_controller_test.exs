defmodule RandomUserPointsWeb.HighScoreControllerTest do
  use RandomUserPointsWeb.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "timestamp is updated on multiple consecutive calls", %{conn: conn} do
      conn1 = get(conn, Routes.high_score_path(conn, :index))
      assert json_response(conn1, 200)["timestamp"] == nil

      conn2 = get(conn, Routes.high_score_path(conn, :index))
      assert json_response(conn2, 200)["timestamp"] != nil
    end
  end
end
