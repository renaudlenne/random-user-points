defmodule RandomUserPoints.ScoreFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `RandomUserPoints.Score` context.
  """

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        points: 42
      })
      |> RandomUserPoints.Score.create_user()

    user
  end
end
